package com.investimentos.repositories;

import com.investimentos.models.SimulacaoR;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRRepository extends CrudRepository<SimulacaoR, Integer> {
}
