package com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Simulacoes_Realizadas")
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Email(message = "O formato do email é inválido")
    private String email;

    @NotNull
    @ManyToOne
    private Investimento investimento;

    @NotNull
    @DecimalMin("1")
    private Integer mesesDeAplicacao;

    @NotNull
    @DecimalMin("100.00")
    private Double dinheiroAplicado;

    @NotNull
    @OneToOne
    private SimulacaoR retornoSimulado;

    public Simulacao() {
    }

    public Simulacao(Integer id, @Email(message = "O formato do email é inválido") String email, @NotNull Investimento investimento, @NotNull @DecimalMin("1") Integer mesesDeAplicacao, @NotNull @DecimalMin("100.00") Double dinheiroAplicado, @NotNull @DecimalMin("0.1") SimulacaoR retornoSimulado) {
        this.id = id;
        this.email = email;
        this.investimento = investimento;
        this.mesesDeAplicacao = mesesDeAplicacao;
        this.dinheiroAplicado = dinheiroAplicado;
        this.retornoSimulado = retornoSimulado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public Integer getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(Integer mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }

    public SimulacaoR getRetornoSimulado() {
        return retornoSimulado;
    }

    public void setRetornoSimulado(SimulacaoR retornoSimulado) {
        this.retornoSimulado = retornoSimulado;
    }
}
