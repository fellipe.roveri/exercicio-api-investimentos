package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.repositories.SimulacaoRRepository;
import com.investimentos.repositories.SimulacaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private SimulacaoRRepository simulacaoRRepository;

    @Autowired
    InvestimentoService investimentoService;

    public Simulacao registrarSimulacao(Simulacao simulacao) throws ObjectNotFoundException {

        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(simulacao.getInvestimento().getId());

        if (investimentoOptional.isPresent()){
            SimulacaoR simulacaoR = new SimulacaoR();
            simulacaoR.setRetorno(calcularRetorno(simulacao));
            SimulacaoR simulacao2 = simulacaoRRepository.save(simulacaoR);
            simulacao.setRetornoSimulado(simulacaoR);
            Simulacao simulacao3 = simulacaoRepository.save(simulacao);
            return simulacao;
        }
        throw new ObjectNotFoundException(Investimento.class, "Investimento não encontrado");
    }

    public Double calcularRetorno(Simulacao simulacao){
        Double indice = Math.pow((1 + (simulacao.getInvestimento().getPorcentagemLucro()/100)),simulacao.getMesesDeAplicacao());

        /*C = inicial * (1+i)^n */
        Double retornoEsperado = simulacao.getDinheiroAplicado() * indice;

        return retornoEsperado;
    }

}
