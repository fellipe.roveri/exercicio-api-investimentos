package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Iterable<Investimento> buscarTodosInvestimentos(List<Integer> investimentoId){
        Iterable<Investimento> investimentoIterable = investimentoRepository.findAllById(investimentoId);
        return investimentoIterable;
    }

    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimento = investimentoRepository.findAll();
        return investimento;
    }

    public Optional<Investimento> buscarPorId(int id) {
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
        return investimentoOptional;
    }

    public Investimento salvarInvestimento(Investimento investimento) {
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Investimento atualizarInvestimento(Investimento investimento) throws ObjectNotFoundException {
        Optional<Investimento> investimentoOptional = buscarPorId(investimento.getId());

        if (investimentoOptional.isPresent()){
            Investimento investimentoObjeto = investimentoRepository.save(investimento);
            return investimentoObjeto;
        }
        throw new ObjectNotFoundException(Investimento.class, "Investimento não encontrado");
    }

    public void deletarInvestimento(Investimento investimento){
        investimentoRepository.delete(investimento);
    }
}