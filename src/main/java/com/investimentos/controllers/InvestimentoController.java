package com.investimentos.controllers;

import com.investimentos.models.Investimento;
import com.investimentos.services.InvestimentoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Investimento> buscarInvestimento(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()){
            return ResponseEntity.status(200).body(investimentoOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Investimento> incluirInvestimento(@RequestBody @Valid Investimento investimento) {
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Investimento> atualizarInvestimento(@PathVariable Integer id, @RequestBody @Valid Investimento investimento){
        investimento.setId(id);
        Investimento investimentoObjeto ;
        try{ investimentoObjeto = investimentoService.atualizarInvestimento(investimento);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(investimentoObjeto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> deletarInvestimento(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()) {
            investimentoService.deletarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(investimentoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}