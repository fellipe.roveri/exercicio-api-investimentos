package com.investimentos.controllers;

import com.investimentos.models.Simulacao;
import com.investimentos.services.SimulacaoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PutMapping()
    public ResponseEntity<Simulacao> realizarSimulacao(@RequestBody @Valid Simulacao simulacao){
        Simulacao simulacaoObjeto;
        try{simulacaoObjeto = simulacaoService.registrarSimulacao(simulacao);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(simulacaoObjeto);
    }
}
