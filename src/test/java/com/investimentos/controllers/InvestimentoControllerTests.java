package com.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.services.InvestimentoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)

public class InvestimentoControllerTests {

    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    Investimento investimento;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        investimento.setNome("FUNDO RV MULTIMERCADO");
        investimento.setDescricao("FUNDO RV MULTIMERCADO 20%ACOES 20%TP 60%RF");
        investimento.setTipoDeRisco(RiscoDoInvestimento.MEDIO);
        investimento.setPorcentagemLucro(0.80);
    }

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testarBuscarTodosInvestimentos() throws Exception {
        Iterable<Investimento> investimentoIterable = Arrays.asList(investimento);
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentoIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/")).
                andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarInvestimento() throws Exception {
        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(investimentoOptional);
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")).
                andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarInvestimentoNOK() throws Exception {
        Optional<Investimento> investimentoOptional = Optional.empty();
        Mockito.when(investimentoService.buscarPorId(2)).thenReturn(investimentoOptional);
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/2"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarIncluirInvestimento() throws Exception {
        investimento.setId(1);
        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id",
                        CoreMatchers.equalTo(1)))  ;
     }

    @Test
    public void testarAtualizarInvestimento() throws Exception {
        String nomeOriginal = investimento.getNome();
        investimento.setNome("FUNDO RV MULTIMERCADO DUNAMIS");

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1").contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("FUNDO RV MULTIMERCADO DUNAMIS")))
        ;
    }

    @Test
    public void testarDeletarInvestimento() throws Exception {
        Optional<Investimento> investimentoOptional = Optional.of(this.investimento);
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(investimentoOptional);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("FUNDO RV MULTIMERCADO")));

        Mockito.verify(investimentoService,Mockito.times(1)).deletarInvestimento(Mockito.any(Investimento.class));
    }
}
