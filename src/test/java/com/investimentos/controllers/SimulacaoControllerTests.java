package com.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(InvestimentoController.class)

public class SimulacaoControllerTests {

    @MockBean
    SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    Simulacao simulacao;
    SimulacaoR simulacaoR;
    Investimento investimento;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        simulacao = new Simulacao();
        simulacaoR = new SimulacaoR();
        simulacao.setEmail("cynthia@gmail.com");
        investimento.setId(1);
        investimento.setNome("POUPANÇA");
        investimento.setDescricao("POUPANÇA");
        investimento.setPorcentagemLucro(0.30);
        investimento.setTipoDeRisco(RiscoDoInvestimento.BAIXO);
        simulacao.setInvestimento(investimento);
        simulacao.setMesesDeAplicacao(4);
        simulacao.setDinheiroAplicado(200.00);
        simulacaoR.setId(1);
        simulacaoR.setRetorno(0.00);
        simulacao.setRetornoSimulado(simulacaoR);
    }

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testarRealizarSimulacao() throws Exception {
        Mockito.when(simulacaoService.calcularRetorno(Mockito.any(Simulacao.class))).thenReturn(simulacaoR.getRetorno());
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.RetornoSimulado.Retorno",
                        CoreMatchers.equalTo(202.4108216161999)))  ;
    }
}
