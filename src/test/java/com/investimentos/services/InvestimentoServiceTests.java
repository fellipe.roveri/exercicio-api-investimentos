package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        investimento.setNome("FUNDO RV MULTIMERCADO");
        investimento.setDescricao("FUNDO RV MULTIMERCADO 20%ACOES 20%TP 60%RF");
        investimento.setTipoDeRisco(RiscoDoInvestimento.MEDIO);
        investimento.setPorcentagemLucro(0.80);
    }

    @Test
    public void testeBuscarTodosInvestimentos(){
        investimentoService.buscarTodosInvestimentos();
        Assertions.assertEquals("FUNDO RV MULTIMERCADO", investimento.getNome());
    }

    @Test
    public void testeBuscarTodosInvestimentosporID(){
        List<Integer> id = new ArrayList<>();
        id.add(1);
        investimentoService.buscarTodosInvestimentos(id);
        Assertions.assertEquals("FUNDO RV MULTIMERCADO", investimento.getNome());
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

        String nome = investimentoService.buscarPorId(1).get().getNome();
        Assertions.assertEquals("FUNDO RV MULTIMERCADO", nome);

        Mockito.verify(investimentoRepository,Mockito.times(1)).findById(1);
     }


     @Test
     public void testarSalvarInvestimento(){
         investimento.setId(1);
         Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);
         Investimento investimentoObjeto  = investimentoService.salvarInvestimento(investimento);

         Assertions.assertEquals(investimento, investimentoObjeto);
         Assertions.assertEquals(investimento.getNome(), investimentoObjeto.getNome());
     }

     @Test
     public void testarAtualizarInvestimento(){
         investimento.setId(1);
         String nomeOriginal = investimento.getNome();
         Optional<Investimento> investimentoOptional = Optional.of(investimento);

         Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);
         Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

         investimento.setNome("FUNDO RV MULTIMERCADO CARTEIRA ITAU");
         Investimento investimentoObjeto2  = investimentoService.atualizarInvestimento(investimento);

         /* Teste do que mudou */
         Assertions.assertEquals("FUNDO RV MULTIMERCADO CARTEIRA ITAU", investimentoObjeto2.getNome());
         /* Teste do que não mudou */
         Assertions.assertEquals("FUNDO RV MULTIMERCADO 20%ACOES 20%TP 60%RF", investimentoObjeto2.getDescricao());
     }

     @Test
    public void testarDeletarInvestimento(){
        investimentoService.deletarInvestimento(investimento);
        Mockito.verify(investimentoRepository,Mockito.times(1)).delete(Mockito.any(Investimento.class));
     }
}
