package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.repositories.InvestimentoRepository;
import com.investimentos.repositories.SimulacaoRRepository;
import com.investimentos.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    @MockBean
    SimulacaoRepository simulacaoRepository;

    @MockBean
    SimulacaoRRepository simulacaoRRepository;

    @Autowired
    SimulacaoService simulacaoService;


    Investimento investimento;
    Simulacao simulacao;
    SimulacaoR simulacaoR;


    @BeforeEach
    public void inicializar() {
        simulacao = new Simulacao();
        simulacaoR = new SimulacaoR();
        investimento = new Investimento();
        simulacao.setEmail("cynthia@gmail.com");
        investimento.setId(1);
        investimento.setNome("POUPANÇA");
        investimento.setDescricao("POUPANÇA");
        investimento.setPorcentagemLucro(0.30);
        investimento.setTipoDeRisco(RiscoDoInvestimento.BAIXO);
        simulacao.setInvestimento(investimento);
        simulacao.setMesesDeAplicacao(4);
        simulacao.setDinheiroAplicado(200.00);
        simulacaoR.setId(1);
        simulacaoR.setRetorno(202.00);
        simulacao.setRetornoSimulado(simulacaoR);
    }

    @Test
    public void testarregistrarSimulacao() {
        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(simulacaoRRepository.save(Mockito.any(SimulacaoR.class))).thenReturn(simulacaoR);
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenReturn(simulacao);

        Assertions.assertEquals("POUPANÇA", investimento.getNome());
        Assertions.assertEquals(202.00, simulacaoR.getRetorno());
        Assertions.assertEquals(4, simulacao.getMesesDeAplicacao());
        }
}
